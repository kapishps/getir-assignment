# getir-assignment

## Setup Instructions

 - Clone the repo
 - Run `npm install` to install the required modules
 - Set MONGODB_URI as environment variable
```bash
export MONGODB_URI=mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true
```
 - Start Server `npm run start`
 - Open http://localhost:3000/getir/ in browser or run curl command to test working
```bash
curl -X GET http://localhost:3000/getir/
```
 - Run `npm run test` to run tests

---
### AWS Endpoint

http://3.23.128.62:3000

### Routes
GET /  
GET /getir  
POST /getir/search  

### Sample Requests

```bash
curl -X POST http://3.23.128.62:3000/getir/search -H 'Content-Type: application/json' -d '{"startDate": "2016-01-26", "endDate": "2018-02-02", "minCount": 2700, "maxCount": 3000}'
```

```bash
curl -X POST http://3.23.128.62:3000/getir/search -H 'Content-Type: application/json' -d '{"startDate": "2016-01-26", "endDate": "2018-13-02", "minCount": 2700, "maxCount": 3000}'
```

### Response Format

Each Response has code and msg. code and its meaning is given below:-

|code| meaning  |
|--|--|
| 0  | Success |
| 1  | Invalid Parameters |
| 2  | Internal Server Error |
| 3  | 404 Not Found|