const express = require('express');
const router = express.Router();
const Records = require("../controllers/records");
const Validator = require('../validators/search-params');

router.get('/', function(req, res, next) {
  res.status(200).send({message : "Welcome to the Getir Service"});
});

router.post('/search', Validator.validateRules, Records.searchRecords);

module.exports = router;
