const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const { connectDB } = require('./utils/db');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const getirRouter = require('./routes/getir');

const app = express();

connectDB(process.env.MONGODB_URI);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/getir', getirRouter);
app.use((req, res,next) => {
  res.status(404).send({ code: 3, msg: 'Error: not found' });
});

module.exports = app;
