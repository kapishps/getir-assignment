const { body} = require('express-validator');

module.exports.validateRules = [
    body('startDate')
      .exists({ checkFalsy: true, checkNull: true })
      .withMessage('must not be empty')
      .bail()
      .trim()
      .isDate({format: 'YYYY-MM-DD', strictMode: true})
      .withMessage('must be a valid date in YYYY-MM-DD'),
    body('endDate')
      .exists({ checkFalsy: true, checkNull: true })
      .withMessage('must not be empty')
      .bail()
      .trim()
      .isDate({format: 'YYYY-MM-DD', strictMode: true})
      .withMessage('must be a valid date in YYYY-MM-DD'),
    body('minCount')
      .exists({ checkFalsy: true, checkNull: true })
      .withMessage('must not be empty')
      .bail()
      .trim()
      .isInt({min: 0})
      .withMessage('must be a valid positive Integer'),
    body('maxCount')
      .exists({ checkFalsy: true, checkNull: true })
      .withMessage('must not be empty')
      .bail()
      .trim()
      .isInt({min: 0})
      .withMessage('must be a valid positive Integer')
  ];