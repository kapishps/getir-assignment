const Mongoose = require('mongoose');

const RecordSchema = new Mongoose.Schema({
  key: { type: String, required: true },
  value: { type: String, required: true },
  createdAt: { type: Date, default: Date.now() },
  counts: [{ type: Number,}]
});

class Record {
  static searchQuery(startDate, endDate, minCount, maxCount) {
    return this.aggregate([
      { $match: { createdAt: { "$gte": startDate, "$lte": endDate } } },
      { $project: { key: 1, createdAt: 1, _id: 0, totalCount: { "$sum": "$counts" } } },
      { $match: { totalCount: { "$gte": minCount, "$lte": maxCount } } }
    ]).exec()
  }
}

RecordSchema.loadClass(Record);

module.exports = Mongoose.model('Record', RecordSchema);;