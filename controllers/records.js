const Record = require('../models/record');
const { validationResult } = require('express-validator')

module.exports.searchRecords = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send({ code: 1, msg: errors.array() });
    }

    let { startDate, endDate, minCount, maxCount } = req.body;
    startDate = new Date(startDate);
    endDate = new Date(endDate);
    minCount = parseInt(minCount);
    maxCount = parseInt(maxCount);

    let data = await Record.searchQuery(startDate, endDate, minCount, maxCount);
    return res.status(200).send({ code: 0, msg: "Success", records: data });
  } catch (err) {
    console.log(err);
    return res.status(500).send({ code: 2, msg: 'Error: ' + err });
  }
};