const request = require('supertest');
const app = require('../app');
const db = require('../utils/db');

beforeAll(async () => {
  await db.connectDB(process.env.MONGODB_URI);
});

afterAll(async () => {
  // Closing the DB connection allows Jest to exit successfully.
  await db.disconnectDB();
});

describe('GET /getir endpoint', () => {
  it('should return 200', async () => {
    const response = await request(app).get('/getir');
    expect(response.status).toBe(200);
  });
});

describe('GET /getir/search endpoint', () => {
  it('should return 404', async () => {
    const response = await request(app).get('/getir/search');
    expect(response.status).toBe(404);
    expect(response.body.code).toBe(3);
  });
});

describe('POST /getir/search with valid params', () => {
  it('should return 200 and valid records', async () => {
    const params = {
      startDate: "2016-01-01",
      endDate: "2021-01-09",
      minCount: 1000,
      maxCount: 3000,
    };
    const response = await request(app).post('/getir/search').send(params);
    expect(response.status).toBe(200);
    expect(response.body.records.length).toBeGreaterThan(0);
    expect(response.body.code).toBe(0);
  });
});

describe('POST /getir/search with invalid date', () => {
  it('should return 400', async () => {
    const params = {
      startDate: "2016-13-01",
      endDate: "2021-01-09",
      minCount: 50,
      maxCount: 3000,
    };
    const response = await request(app).post('/getir/search').send(params);
    expect(response.status).toBe(400);
    expect(response.body.code).toBe(1);
  });
});

describe('POST /getir/search with invalid minCount', () => {
  it('should return 400', async () => {
    const params = {
      startDate: "2016-13-01",
      endDate: "2021-01-09",
      minCount: '-8',
      maxCount: 3000,
    };
    const response = await request(app).post('/getir/search').send(params);
    expect(response.status).toBe(400);
    expect(response.body.code).toBe(1);
  });
});

describe('POST /getir/search with missing startDate', () => {
  it('should return 400', async () => {
    const params = {
      endDate: "2021-01-09",
      minCount: 1000,
      maxCount: 3000,
    };
    const response = await request(app).post('/getir/search').send(params);
    expect(response.status).toBe(400);
    expect(response.body.code).toBe(1);
  });
});