const Mongoose = require('mongoose');

const connectDB = async (MONGODB_URI) => {
  try {
    await Mongoose.connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });
  } catch (err) {
    console.log(err);
  }
};

const disconnectDB = async () => {
  await Mongoose.disconnect();
};

module.exports = { connectDB, disconnectDB };